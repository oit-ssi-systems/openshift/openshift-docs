# Duke University OKD Documentation

Duke University OKD: A managed platform for developing and deploying webapps in your choice of programming languages, versions and frameworks - on-demand.

* Development Environment in Minutes
* Choice of Languages, Tools, and Frameworks
* Centrally Managed Infrastructure

## Features and Benefits

**Focus On Development:** Worry about your application rather than infrastructure management.  We'll manage the servers - you can focus on the code.

**On-Demand Environment:** Projects can be created instantly.

**Choice of Languages:** Choose from a list of popular programming languages and frameworks. Numerous community-supported starter code will get you going with other software options.

**Enterprise Infrastructure:** The service is hosted on server class hardware and is maintained by OIT personnel whom already provide enterprise-level management and support to critical university software applications.

## What is OKD

[OKD](https://okd.io) is a distribution of [Kubernetes](https://kubernetes.io) optimized for continuous application development to enable rapid application development, easy deployment and scaling.

## Getting Started

For learning, experimentation or other non-production use cases, please use the 
- [Development OKD4 Cluster](https://console.apps.dev.okd4.fitz.cloud.duke.edu/)

For new projects please use:
- [Production OKD4 Cluster](https://console.apps.prod.okd4.fitz.cloud.duke.edu/)

You can create a new project with the [OIT Cloud Registrar](https://cloud.duke.edu). A Duke FundCode is required.

For legacy projects, the [OKD3 Management Console](https://manage.cloud.duke.edu) is available.

## How to get Help

Kubernetes and OKD can have a bit of a learning curve when getting started. [Find out how to get help.](getting-help.md)
