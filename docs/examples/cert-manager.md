# Example Issuers

!!! info
    - Duke Network Application: a resource that is addressable only from Duke Networks
    - Globally Available Application: a resource that is addressable from anywhere

The important differences between these two Issuers is the necessary label in the `ingressTemplate` key for the Globally Available Application. This is needed so that the external ingress is the one that picks up your ACME solver requests.


## Example cert-manager Issuer for a Duke Network Application

```yaml
---
# This is an issuer for local routes
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  labels:
    app: cert-manager-openshift-routes
  name: locksmith-issuer
spec:
  acme:
    email: email.address@duke.edu # this address will be in locksmith for administrative purposes.
    preferredChain: ''
    privateKeySecretRef:
      name: something-unique-for-locksmith-key  # This is an automatically generated key
    server: 'https://locksmith.oit.duke.edu/acme/v2/directory'
    solvers:
      - http01:
          ingress: {}
status: {}
```

## Example cert-manager Issuer for a Globally Available Application

```yaml
---
# this is an issuer for external/off-campus routes
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  labels:
    app: cert-manager-openshift-routes
  name: locksmith-issuer-external
spec:
  acme:
    email: email.address@duke.edu # this address will be in locksmith for administrative purposes.
    preferredChain: ''
    privateKeySecretRef:
      name: something-unique-for-locksmith-key  # This is an automatically generated key
    server: 'https://locksmith.oit.duke.edu/acme/v2/directory'
    solvers:
      - http01:
          ingress:
            # This ensures that your ACME Challenge pods are assigned to the appropriate router.
            ingressTemplate:
              metadata:
                labels:
                  cloud.duke.edu/route-type: external
status: {}
```
