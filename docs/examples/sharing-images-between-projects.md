# Sharing Images Between Projects

Following the concept of [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), it may be desirable for you to share container images between your projects.

Consider two projects, Project Source and Project Destination.  Project Source has an image `awesome-image:1.0` that needs to be available in Project Destination.

## Add "Image-Puller" permissions

In order to list and pull images from Project Source, a user or service account in Project Destination needs to have the `system:image-puller` role in Project Source.

!!! note
    The `default` service account in Project Destination is the most likely candidate for these permissions, unless you're doing something tricky.

### Add permissions using the Web UI

Using the OKD Web UI in your browser, appropriate permissions within Project Source can be added to, for example, the `default` service account for Project Destination.

1. Open Project source
1. Select `Resources` from the left-hand side
1. Select `Membership` to manage permissions for the project
1. Select the `ServiceAccounts` tab
1. Click "Edit Membership" from the top right corner
1. On the bottom line of the service accounts list, select the first drop down and pick `Project Destination` from the list (substitute your own destination project of course)
1. For the second drop down, select the `Default` service account (or again, substitute your own).
1. Select the third drop down, and select the `system:image-puller` role to add the image pull permissions.
1. Click `Add` from the right-hand side to apply the permissions, and then click `Done Editing` at the top.

### Add permissions using the cli

Using the `oc` command from your terminal ([related: how to install the oc command](https://docs.okd.io/latest/cli_reference/get_started_cli.html#installing-the-cli)), you can create the appropriate roleBinding between the `default` serviceAccount and the `system:image-puller` clusterRole.

```bash
# Bind the `system:image-puller` role for the `default` account in the Project Destination (ie: project-destination) project.

# Change to the Project Source (ie: project-source) project

$ oc project project-source

# Create the roleBinding
$ oc policy add-role-to-user system:image-puller project-destination/default

# Check the roleBinding
$ oc get rolebindings | grep project-destination
system:image-puller-0   /system:image-puller    project-destination/default
```

## Reference the image in the deployments

The deployments or deploymentConfigs in Project Destination must reference awesome image using the format `project/image_name`.  For example, a deploymentConfig would reference the awesome image in `.spec.template.spec.containers[0].image` like so:

```yaml
apiVersion: apps.openshift.io/v1
kind: DeploymentConfig
spec:
  template:
    spec:
      containers:
        - image: project-source/awesome-image:1.0
```

!!! note
    `containers[0]` in the example above is just the first container in the `containers` list.  Substitute the appropriate container for your use case.
