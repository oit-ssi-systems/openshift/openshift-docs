# Example Routes

!!! info
    - Duke Network Application: a resource that is addressable only from Duke Networks
    - Globally Available Application: a resource that is addressable from anywhere.
    N.B. There is no wildcard route available externally. Globally Available Applications must have their own DNS entries.

## ACME enabled Routes

The important thing to note here for a Globally Available Application is the presence of the specific label `cloud.duke.edu/route-type=external` and a reference to your [Issuer](/examples/cert-manager/) with the same label in its `ingressTemplate`.

### ACME enabled OKD4 Route for a Duke Network Application

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    cert-manager.io/issuer-kind: Issuer
    cert-manager.io/issuer-name: locksmith-issuer
    app: app-name
  name: app-name
  namespace: app-namespace
spec:
  host: appname.somewhere.duke.edu
  port:
    targetPort: 80-tcp
  tls:
    termination: edge
    insecureEdgeTerminationPolicy: Redirect
  to:
    kind: Service
    name: app-name
    weight: 100
  wildcardPolicy: None
```

### ACME enabled OKD4 Route for a Globally Available Application

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  labels:
    cloud.duke.edu/route-type: external # this is required for the route to be picked up by the external router
  annotations:
    cert-manager.io/issuer-kind: Issuer
    cert-manager.io/issuer-name: locksmith-issuer-external # be certain this references your external Issuer
    app: app-name
  name: app-name-external
  namespace: app-namespace
spec:
  host: appname.somewhere.duke.edu
  port:
    targetPort: 80-tcp
  tls:
    termination: edge
    insecureEdgeTerminationPolicy: Redirect
  to:
    kind: Service
    name: app-name
    weight: 100
  wildcardPolicy: None
```

## Routes with static certificates

!!! info
    Note that these examples would require that a certificate be placed into the route manually. This process is outlined in the upstream documentation [Creating an edge route with a custom certificate](https://docs.okd.io/latest/networking/routes/secured-routes.html#nw-ingress-creating-an-edge-route-with-a-custom-certificate_secured-routes)

### OKD4 Route for a Duke Network Application

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    app: app-name
  name: app-name
  namespace: app-namespace
spec:
  host: appname.somewhere.duke.edu
  port:
    targetPort: 80-tcp
  tls:
    termination: edge
    insecureEdgeTerminationPolicy: Redirect
  to:
    kind: Service
    name: app-name
    weight: 100
  wildcardPolicy: None
```

### OKD4 Route for a Globally Available Application

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  labels:
    cloud.duke.edu/route-type: external # this is required for the route to be picked up by the external router
  annotations:
    app: app-name
  name: app-name-external
  namespace: app-namespace
spec:
  host: appname.somewhere.duke.edu
  port:
    targetPort: 80-tcp
  tls:
    termination: edge
    insecureEdgeTerminationPolicy: Redirect
  to:
    kind: Service
    name: app-name
    weight: 100
  wildcardPolicy: None
```