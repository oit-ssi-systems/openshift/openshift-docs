# Prometheus monitoring for user-defined workloads

## Service based

This is useful if you're gathering metrics for the ENTIRE application. The scrape will target the service load balancer so therefore will be load balanced across all pods. 

```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: demo-servicemonitor
  namespace: demo-namespace
spec:
  jobLabel: demo-job # this is applied to the metrics during the scrape
  endpoints:
	  - path: /metrics # this is the convention but must be specified
	    port: web      # this likely needs to be a named port, not a number
  namespaceSelector:
    matchNames:
    	- demo-namespace
  selector:
    matchLabels:
      app: demo-app  # note: this is the label on the service to scrape

```

## Pod Based

This is useful if you have metrics that are per-pod or per unique runtime environment for your application.

```yaml
apiVersion: monitoring.coreos.com/v1
kind: PodMonitor
metadata:
  name: demo-podmonitor
  namespace: demo-namespace
spec:
  jobLabel: demo-job # this is applied to the metrics during the scrape
  podMetricsEndpoints:
	  - path: /metrics # this is the convention but must be specified
	    port: web      # this likely needs to be a named port, not a number
  namespaceSelector:
    matchNames:
    	- demo-namespace
  selector:
    matchLabels:
      app: demo-app  # note: this is the label on the pods you'd like to scrape
```

# References

- [ServiceMonitor](https://prometheus-operator.dev/docs/operator/design/#servicemonitor)
- [PodMonitor](https://prometheus-operator.dev/docs/operator/design/#podmonitor)
