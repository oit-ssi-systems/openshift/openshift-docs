# Duke Shibboleth in OKD/Kubernetes

This guide describes how to setup a Shibboleth SP (ie: protect a website with Shib) using container best practices with OKD (OpenShift) or Kubernetes.

## TL;DR

"Too Long; Didn't Read" - Just want to jump in and get started, and skip all the tedious "reading" and "learning" below?  Check out the Duke OpenShift Users Group [Community Supported Shibboleth](https://gitlab.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth) project!

## Best Practices

The "Best Practices" assumed for this guide are:

1.  Containers run a single process (ie: Apache and Shibboleth each have their own containers)
2.  Containers run as non-root users, and support [OKD's arbitrary UID assignment](https://docs.okd.io/latest/creating_images/guidelines.html#openshift-specific-guidelines)
3.  Container images are generic, and configuration is managed by ConfigMaps and Secrets

Best practices are described here, but example Dockerfiles and DeploymentConfigs that do this already are below.

### 1: Containers run a single process

In keeping with container best practices, Apache and Shibboleth will each run in a single container.  In order to facilitate this, the containers must be co-located in a single [Pod](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/) and share the Shibboleth socket and shibboleth2.xml configration file.

The socket must be shared in order to allow mod_shib, installed in the Apache container, to communicate with the Shibboleth process running in the other container.

In addition, mod_shib apparently needs to be able to read the shibboleth2.xml file to tell Apache how to respond to requests.

### 2: Containers run as non-root users

It's good security practice to run container processes as unprivileged users, and in fact, OKD _requires_ that containers be run without root, going so far as to assign randomized UIDs to the user running processes in the container.  This requires a few changes to how the Apache and Shibboleth images run.

We'll focus on the Shibboleth container, as Apache is relatively well-documented elsewhere.

For the Shibboleth container, the user running the shibd process must have read/write access to `/var/run/shibboleth` and `/var/cache/shibboleth`, and read access to `/etc/shibboleth`.  OKD automatically assigns the random user running in the container to the root group, so we can use that to allow access even without knowing the UID ahead of time.

Within the Docker image, recursively change the group ownership of the three directories above to the root group (`chgrp -R root <directory>`) and change the mode to the desired read/write setup (`chmod g+r <directory>` or `chmod g+rw <directory>`).

### 3: Configuration is managed by ConfigMaps and Secrets

There are only a few bits of configuration required to customize generic Apache and Shibboleth container images to work for a specific website/application.

1.  shibboleth2.xml (both containers)
2.  attribute-map.xml/attribute-policy.xml (optional, Shibboleth contianer)
3.  Shibboleth SP TLS certificates (Shibboleth container)
4.  Apache ServerName configuration (Apache container)

_shibboleth2.xml_ - The shibboleth2.xml file is the primary configuration for the Shibboleth SP, and both the Shibboleth and Apache contianers must be able to read it.  For the most part, the default below can be used, changing only the EntityID to that which is registered with the Shibboleth IDPs.

_attribute-map.xml/attribute-policy.xml_ - These are optional files that can change how attributes are parsed by Shibboleth, and need to be readable within the Shibboleth container.

_SP TLS certificates_ - The SP key and certificate is required to identify the SP to the IDP.  These are stored as a Secret in OKD/Kubernetes for security, and need to be readable within the Shibboleth container.

These can be self-signed certificates, and should _not_ be the same TLS certificates used for serving HTTPS traffic.

The Shibboleth Wiki has documentaton on [how to create a self-signed certificate](https://wiki.shibboleth.net/confluence/display/CONCEPT/SAMLKeysAndCertificates#SAMLKeysAndCertificates-CreatingaSAMLKeyandCertificate) that can be used to generate the SP TLS certificates.

_Apache ServerName configuration_ - If using the default OKD router to offload TLS handling, the Apache container needs a configuration file to tell it what hostname it needs to use when handling the Shibboleth ACS URL.  This configuration file contains the fllowing:

```
ServerName https://your.entity.id:443
UseCanonicalName On
```

## Creating the Container Images

Two container images are needed to create the containers running Shibboleth, one with Apache and mod_shib installed, and a second with Shibboleth installed.

There are example Dockerfiles available for building these images in the [Duke Shibboleth Container Project in Duke's Gitlab](https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container).

_Apache_

[https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container/blob/master/Dockerfile-httpd](https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container/blob/master/Dockerfile-httpd)

The example Apache Dockerfile uses OKD's `centos/httpd-24-centos7` as a base image, and just adds the Shibboleth package to ensure mod_shib is installed.

_Shibboleth_

[https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container/blob/master/Dockerfile-shibboleth](https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container/blob/master/Dockerfile-shibboleth)

The example Shibboleth Dockerfile is based on the `centos:centos7` base image, and installs Shibboleth and configures Duke's IDP metadata, sets file permissions, sets the `LD_LIBRARY_PATH` for Shibboleth's custom libcurl package, and changes logging to STDOUT.

_TIP:_ Theoretically, you could do this with a single container image that has both Apache and Shibboleth installed, create two containers from the image and specify which process to start for each container in the pod.  The benefits may not be worth the effort, however.

## Creating the ConfigMaps and Secrets

As noted above, Shibboleth and Apache require some config files and TLS certificates to work correctly.

_Shibboleth_

Shibboleth requires the shibboleth2.xml file and SP TLS certificates to work.  Optionally, other configuration files can be specified, such as the attribute-map.xml.

Create a shibboleth2.xml file, or modify the [example shibboleth2.xml file](https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container/blob/master/shibboleth2.xml-example) to suit your needs.  Then, create a configMap:

`oc create configmap shib-config --from-file=shibboleth2.xml`

!!! note
    If you wish to add more configuration files for Shibboleth, you can add them to the same config map by adding more `--from-file=<filename>` arguments (eg: `oc create configmap shib-config --from-file=shibboleth2.xml --from-file=attribute-map.xml`)

Shibboelth also needs SP TLS certificates to identify itself to the IDP.  The TLS certificate itself is also provided to Duke's IDP when registering the SP.

Generate real or self-signed TLS certificates to use for your SP.  Then, create the TLS Secret with the Key and Cert:

`oc create secret tls --cert=sp-cert.pem --key=sp-key.pem`

The "sp-cert.pem" and "sp-key.pem" should be replaced with the actual filename of your TLS cert and key.

_Apache_

As mentioned above, when using the OKD router to offload TLS processing, Apache must be configured to know what ServerName to use.  Create a file named servername.conf with the following:

```
ServerName https://your.entity.id:443
UseCanonicalName On
```

Then, create a configMap for the Apache config:

`oc create configmap apache-config --from-file=servername.conf`

Once again, if you have other Apache configurations that you'd like to include, you can include them by adding additional `--from-file=<filename>` arguments.

## Creating a DeploymentConfig

!!! note
    [DeploymentConfigs are OKD-specific](https://docs.okd.io/latest/architecture/core_concepts/deployments.html#deployments-and-deployment-configurations), and do not exist within Kubernetes, but the resulting deployment can be adjusted to work for Kuberentes.

When creating a DeploymentConfig, there are three important pieces:

1.  "spec.template.spec.containers" should contain both the Apache and Shibboleth Containers
2.  Volumes should exist for each of the ConfigMaps, Secrets and the Shibboleth socket.
3.  Volume mounts for each of the volumes should be present in the containers as described above (ie, shibboleth2.xml is required in both, servername.conf is required only in the Apache container, etc..)
4.  The Shibboleth socket should make use of the Kubernetes `emptyDir` volume type ([emptyDir](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir)):

```
volumes:
- emptyDir: {}
  name: volume-shib-socket
```

!!! note
    When creating a volume mount for the files, it's important to use the `subPath:` parameter to mount them as files.  Otherwise, the mounts will overwrite the entire directory.  Example:

```
- mountPath: /etc/shibboleth/shibboleth2.xml
  name: volume-shib-2-xml
  readOnly: true
  subPath: shibboleth2.xml
```

Without the `subPath: shibboleth2.xml`, all the files in /etc/shibboleth would be hidden with the exception of the shibboleth2.xml file.

An [example deployment config in the gitlab repo](https://gitlab.oit.duke.edu/devil-ops/duke-shibboleth-container/blob/master/openshift/deploymentConfig.yaml) highlights these and can be used as a reference when creating a real DeploymentConfig.
