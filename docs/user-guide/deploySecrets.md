# OKD Deploy Secrets

OKD requires a `source secret` to pull code from a private git repository using a git `deploy key`.

## Generate a key pair

If you don't already have a deploy key to use, generate a new keypair:

```
$ ssh-keygen -t ed25519 -f openshift_ed25519
```

This will generate two files, a `public key` and a `private key`.

## Upload your public key

_Deploy Keys with Gitlab_

To upload your `public key` as a deploy key with Gitlab:

1.  Go to your repository in Gitlab
2.  Select the "Settings" item from the navbar
3.  Select the "Repository" sub-item under "Settings"
4.  Expand the "Deploy Keys" item on the page
5.  Paste an SSH `public key` for your project, or select an existing one from one of the list.

_Deploy Keys with Github_

To upload your `public key` as a deploy key with Github:

1.  Go to your repository in Github
2.  Select the "Settings" tab from the top
3.  Select the "Deploy keys" item in the navpar
4.  Click the "Add deploy key" button
5.  Paste an SSH `public key` for your project.

The `private key` can then be used with the `--source secret` for OKD when creating a new app.

## Create a secret with your private key

Source secrets are based on the project-level, so first, change to the project where you are working.

```
oc project <projectname>
```

To create a source secret with your `private key`:

```
oc create secret generic <secret_name> \
   --from-file=ssh-privatekey=<path/to/ssh/private/key> \
   --type=kubernetes.io/ssh-auth
```

The "secret_name" can be whatever you will call the secret later, eg: gitdeploykey

Optionally, annotate it with a `source-secret-match-uri-`, to allow build configs to automatically use the secret.

```
oc annotate secret <secret_name> 'build.openshift.io/source-secret-match-uri-1=ssh://gitlab.oit.duke.edu/*'
```

After being annotated, any new buildConfig will use the source secret automatically, but existing ones need to be updated.  Edit the buildConfig with `oc edit buildConfig <buildConfig name>`, and find `.spec.source`.  Under that, add your source secret:

```
spec:
  source:
    sourceSecret:
      name: <your secret name>
```

An example buildConfig `.spec.source` section might look like so:

```
  source:
    git:
      ref: staging
      uri: git@gitlab.oit.duke.edu:oit-astronauts/openshift-docs.git
    sourceSecret:
      name: my-deploy-key
    type: Git
```

!!! note
    When you create your new application, you must specify the repo in the format "ssh://git@gitlab.oit.duke.edu/my-project/my-repo.git" for the auto-secret to work.

See the [docs for source-secret-match-uri](https://docs.openshift.com/container-platform/latest/dev_guide/builds/build_inputs.html#automatic-addition-of-a-source-secret-to-a-build-configuration)

## Link it to the builder service account

```
oc secrets link builder <secret_name>
```

## Helper Script

This script will do everything from the "Create a secret with your private key" step onward if a secret with the specified name doesn't exist in the current OKD Project.

```
#!/usr/bin/env bash

# Pick a name for your builder Deploy Secret
BUILDER_SECRET='secret_name'
KEYFILE='path/to/the/ssh/private/key'


if ! oc get secret $BUILDER_SECRET
then
  oc create secret generic $BUILDER_SECRET \
    --from-file=ssh-privatekey=${KEYFILE} \
    --type=kubernetes.io/ssh-auth

  oc annotate secret $BUILDER_SECRET 'build.openshift.io/source-secret-match-uri-1=ssh://gitlab.oit.duke.edu/*'

  oc secrets link builder $BUILDER_SECRET
fi

```
