# SSL Certificate Automation for Locksmith

!!! note
    In OKD4 we've moved to a cluster wide service based on [cert-manager](https://cert-manager.io/docs/) for issuing certificates. If you're migrating your applications you will not need to migrate your openshift-acme deployments to the new environment.

### Basic Certificate Info for OKD4

The process for configuring certificate automation in OKD4 is as follows:

1. Configure your dns records to resolve to the appropriate [cluster ingress address](/cluster-details/okd4/cluster-overview/).
1. Create an [Issuer](/examples/cert-manager/) that references the email address responsible for the ongoing maintenance of the certificate.
1. Create a [Route](/examples/okd4-routes/) appropriate for your application with annotations to indicate which Issuer to use for certificate creation.
