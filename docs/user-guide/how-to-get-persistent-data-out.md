# Getting your data out of a persistent volume

If you'd like to have a copy of the data from a persistent volume in your Duke OKD project, you can use the following steps to effectively download a copy of anything in your persistent volume.

These are adapted from the [upstream project documentation on the topic](https://docs.okd.io/3.11/day_two_guide/environment_backup.html#backing-up-pvc_environment-backup).

**List the pods**

List the pods in your project, and select the pod that has the data that needs to be extracted.

```
 $ oc get pods

NAME                                 READY     STATUS      RESTARTS   AGE
mypod-1-vhvhg                      1/1       Running     0          18h
```

**Find the persistent volume where the data is mounted**

Using the `oc describe pod` command, find the mount point where the volume is mounted.

```
# Here we see the persistent volume "mydata-pv" is mounted on /data inside our selected container.
$ oc describe pod mypod-1-vhvhg

   ...
   Mounts:
         /data from mydata-pv (rw)
   ...
```

**Copy the data out of the pod with `oc rsync`**

`oc rsync` the data into a local directory on your system using the format `podname:/path/to/mountpoint` to reference the source data.

```
$ mkdir mypod-data
$ oc rsync mypod-1-vhvhg:/data mypod-data

data/awesome-stuff.md
data/application-config.conf
```

The result is that the directory `mypod-data` contains a point in time copy of what was inside the persistent volume `mydata-pv` on the pod `mypod-vhvhg` in the `/data` directory.

## Upload data to a persistent volume

`oc cp` prefers to upload whole directories or individual filenames, it is not great with wildcards.

Using the same method as above to find the relevant pod name, upload with `oc cp`

```
$ oc cp dir/ mypod-1-vhvhg:/path/for/files
```