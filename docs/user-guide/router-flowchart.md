```mermaid
graph LR;
 client([client])-->dnsrr[yourapp.duke.edu<br>CNAME<br>ingress.prod.okd4.fitz.cloud.duke.edu] -->ingress[Duke Network<br>OKD Router];
 ingress-->|routing rules|service[Service];
 subgraph cluster
 ingress;
 service-->pod1[Pod];
 service-->pod2[Pod];
 end

 classDef plain fill:#F3F2F1,stroke:#E2E6ED,stroke-width:4px,color:#005587;
 classDef k8s fill:#005587,stroke:#E2E6ED,stroke-width:4px,color:#E2E6ED;
 classDef cluster fill:#F3F2F1,stroke:#005587,stroke-width:2px,color:#326ce5;
 classDef pod1 fill:#339898
 classDef pod2 fill:#1D6363
 classDef dnsrr fill:#262626

 class pod1 pod1;
 class pod2 pod2
 class dnsrr dnsrr;
 class dnsrr,ingress,service,pod1,pod2 k8s;
 class client plain;
 class cluster cluster;
```