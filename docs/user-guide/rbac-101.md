Role-based Access Control 101
=============================

OKD and Kubernetes use RBAC, or [Role-based Access Control](https://docs.okd.io/3.11/admin_guide/manage_rbac.html) to allow fine-grained control of who can do what in a complicated cluster.


Quick and Dirty RBAC Overview
-----------------------------

1.  Permissions are based on `verbs` and `resources` - eg. "create group", "delete pod", etc..
2.  Sets of permissions are grouped into `roles` or `clusterroles`, the latter being cluster-wide, obvs.

    eg: `oc create clusterrole group-sync --verb=create,get,watch,patch --resource=group`

3.  Roles and Clusterroles are applied to `groups` and `serviceAccounts`, or, if you want to do it all wrong, individual `users`.

    eg: `oc adm policy add-cluster-role-to-user group-sync system:serviceaccount:astronaut-ops:sync-group-manager`

If you were following along with the examples, you'll see we created a clusterRole, "group-sync", and allowed it to create, get, watch and patch groups (ie: make, read, watch for changes, and update them).  We then added that clusterRole to the sync-group-manager serviceAccount created earlier, to allow that service account to do those things.

Go fourth.  Read docs.  Secure your stuff.
