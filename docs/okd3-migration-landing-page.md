---
title: OKD3 Migration Landing Page
---

### The OIT OKD3 service has been decomissioned.

You're on this page because you accessed a URL that was configured on the OKD3 cluster. That Service was discontinued on July 1 2024 in tandem with the deprecation of RHEL and CentOS version 7.

If you are the maintainer of this application please [get in touch with the OIT SSI Linux team](https://oit.duke.edu/help/) that maintains this service. We will be able to provide you with much of your prior configuration and data and help you get your application running in the new OKD4 cluster.

If you are a user of the application get in touch with the maintiner of the application you were using to ask them if there is a new URL or if they need to migrate the service to the new cluster.
