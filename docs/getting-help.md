# How to Get Help

Duke OKD is a self-service platform, but there are many ways to get help learning how to do something or troubleshooting a problem.

## Duke OpenShift Users Group

The Duke OpenShift Users Group (or DOUG) is a community group open to all Duke University and Duke Health folks interested in learning and sharing with other OKD/OpenShift users at Duke.

The group consists of:

* a mailing list (openshift-users@duke.edu) to notify members of future group meetings.
* a Microsoft Teams presence @ [Duke Openshift Users Group Team](https://teams.microsoft.com/l/team/19%3a8fc117cd5cf14856860c1b366507ad95%40thread.skype/conversations?groupId=f66622b8-ed9e-4fa4-927e-137089e86a0f&tenantId=cb72c54e-4a31-4d9e-b14a-1ea36dfac94c) where members can communicate with each other in real time
* Repositories in the [Duke Openshift Users Gitlab Group](https://gitlab.oit.duke.edu/duke_openshift_users)

Members of the Group should read the [Code of Conduct](https://gitlab.oit.duke.edu/duke_openshift_users/information/blob/master/CodeOfConduct.md).

## OKD Office Hours

OIT Hosts weekly "OKD Office Hours" on Tuesdays at 1:30pm via [Zoom Teleconference](https://duke.zoom.us/j/500705833).

!!! note
    Times for the Office Hours meeting may vary. You can verify in the [Office Hours Chat](https://teams.microsoft.com/l/channel/19%3a9d2e0a37582a4a7693cea947ebafe436%40thread.skype/Office%2520Hours%2520Chat?groupId=f66622b8-ed9e-4fa4-927e-137089e86a0f&tenantId=cb72c54e-4a31-4d9e-b14a-1ea36dfac94c).

## Official OKD and Kubernetes Documentation

In addition to Duke OKD-specific documentation available on this site, both OKD and Kubernetes have extensive documentation of their features.

* [Official OKD Documentation](https://docs.okd.io/)
* [Official Kubernetes Documentation](https://kubernetes.io/docs/home/)
