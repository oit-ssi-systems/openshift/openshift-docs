# OKD4 @ Duke

For OKD4 we have the following 2 clusters for general use.

- Development [okd4-dev](https://console.apps.dev.okd4.fitz.cloud.duke.edu/)

  - web console: [https://console.apps.dev.okd4.fitz.cloud.duke.edu/](https://console.apps.dev.okd4.fitz.cloud.duke.edu/)
  - api address: `https://api.dev.okd4.fitz.cloud.duke.edu:6443`
  - wildcard address: `*.apps.dev.okd4.fitz.cloud.duke.edu`
  - route ingress address (for your cnames):
    - Duke Network Application: ingress.dev.okd4.fitz.cloud.duke.edu
    - Globally Available Application: external.ingress.dev.okd4.fitz.cloud.duke.edu
  - image registry: `registry.apps.dev.okd4.fitz.cloud.duke.edu`

***

- Production [okd4-prod](https://console.apps.prod.okd4.fitz.cloud.duke.edu/)

  - web console: [https://console.apps.prod.okd4.fitz.cloud.duke.edu/](https://console.apps.prod.okd4.fitz.cloud.duke.edu/)
  - api address: `https://api.prod.okd4.fitz.cloud.duke.edu:6443`
  - wildcard address: `*.apps.prod.okd4.fitz.cloud.duke.edu`
  - route ingress adddress (for your cnames): 
    - Duke Network Application: ingress.prod.okd4.fitz.cloud.duke.edu
    - Globally Available Application: external.ingress.prod.okd4.fitz.cloud.duke.edu
  - image registry: `registry.apps.prod.okd4.fitz.cloud.duke.edu`

***

!!! note "Note on Globally Available Application DNS records"
    
    Be sure when you create your CNAMEs to the external ingress address and that you choose the Internal + External view so that the record is published to both DNS views. N.B. There is no wildcard route available for Globally Available Applications.

Both clusters are located in the Fitz East datacenter right alongside each other for as seamless as possible testing and developement of application behavior.

***

Network:

- [OIT-DATACENTER-OPENSHIFT-01](https://cartographer.oit.duke.edu/ipv4/subnets/2499) : `10.138.5.0/24`

Storage:

- storageclasses:

  - nfs-client: NFS PVCs provided by oit-nas-fe11

    - can do RWX/ReadWriteMany access mode

  - thin: VMDK PVCs

    - provides a virtual block backed filesystem suitable for non-shared pod usage.

***

Quotas:

- default-quota:

  - 2 cores / 2GB of RAM

- kubernetes-object-counts:

  - limit of

    - 100 configmaps
    - 100 secret

- openshift-object-counts:

  - limit of

    - 70 imagestreamtags

Default Requests/Limits:

- limitrange:

  - defaultRequest:

    - cpu: 50m (down from 250m in okd3)
    - memory: 25Mi (same as okd3)

  - default limits:

    - cpu: 7 (same as okd3)
    - memory: 8Gi (down from 12Gi in okd3)

Documentation on quotas:

  - [on docs.okd.io](https://docs.okd.io/4.11/nodes/clusters/nodes-cluster-resource-configure.html#nodes-cluster-resource-configure-about_nodes-cluster-resource-configure)

There will be more details here soon.

  - [NetworkPolicies](https://docs.okd.io/4.9/networking/network_policy/about-network-policy.html)