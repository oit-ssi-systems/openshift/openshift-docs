# Persistent Storage Options

Duke OKD/OKD supports persistent storage though [Kubernetes Storage Classes](https://kubernetes.io/docs/concepts/storage/storage-classes/) and provisions persistent storage for your applications dynamically when you create a [Persistent Volume Claim](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims).

_Requesting Persistent Volumes via Persistent Volume Claims is outside the scope of this document.  The [Kubernetes Persistent Volume Documentation](https://kubernetes.io/docs/concepts/storage/persistent-volumes/) is a good place to start for that._

## Supported Storage Classes

Currently, Duke OKD has one Storage Class available:

1. standard (default)

You can see the available Storage Classes with the `oc get storageclasses` command.

## What is the standard storage class

Generally, the standard storage class should work for most applications.  This storage class option is the default storage class, and will be selected automatically if you do not specify a storage class when creating a Persistent Volume Claim.

The Standard storage class is an NFS shared filesystem, so multiple pods can write to it at once.
