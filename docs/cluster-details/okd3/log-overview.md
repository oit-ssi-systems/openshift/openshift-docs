# Log Aggregation

Logs from all pods (cronjobs, builds, deployments, etc) are aggregated in the OKD cluster and are available via an [integrated kibana instance](https://kibana.cloud.duke.edu). These logs are stored for a max of 30 days on a best-effort basis.

Administrative and Application logs are also aggreagated to the OIT Splunk instance for use by the Information Technology Security Office. User access to these logs can be facilitated on a case-by-case basis.
