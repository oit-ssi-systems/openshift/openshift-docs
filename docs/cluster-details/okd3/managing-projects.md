# Requesting and Managing Projects

Projects are created and managed in Duke OKD using the [Cloud Registrar application](https://cloud.duke.edu/).

To create a project, you will need a name for the project and a fund code. For OKD, choose `openshift` as the platform. Optionally, a [Group Manager](https://groups.oit.duke.edu/groupmanager/) Group can be authorized for access to the project.

!!! info "Setting Group Sync Options"
    For groups to be available for use with Duke OKD, the `WIN AD` sync option must be enabled in Group Manger.
