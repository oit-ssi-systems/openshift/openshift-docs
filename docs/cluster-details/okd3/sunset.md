# OKD3 Cluster Sunset Details

!!! Work-in-Progress

	This document is a Work in Progress and will change over time as we develop a plan to sunset the OKD v3.11 cluster. If you have any questions see the Questions section below on how to get some answers.

History:

OKD v3.11 is being slowly phased out by the software vendor and is no longer maintained. The commercially derived product OCP (Openshift Container Platform) v3.11 went EOL in June 2022 and reaches end of Extended Life Phase June 2024.

All new projects should be created in the OKD4 clusters. Our OKD v4.X has been online and operated as a production service since Mid 2022. 

## Phase 1 - Disable User Project Provisioning - Summer 2023

Beginning on 1 July 2023 new project provisioning will be disabled. Projects can still be created by cluster administrators on a case-by-case basis. To create a project in OKD3 please submit a ticket with business justification to OIT-Unix-Systems in Service Now or contact the [OIT Service Desk](https://oit.duke.edu/help).

## Phase 2 - No New Projects - Q4 2023

Beginning on 1 Jan 2024 no new projects will be provisioned into OKD v3.11. 

## Sunset - 1 July 2024

- Orphan project process TBD

## Questions?

Please feel free to send the Systems-UNIX-OIT service now group a ticket or just submit a ticket via the [OIT Service Desk](https://oit.duke.edu/help) asking for help with the Duke University OKD v3.11 cluster.