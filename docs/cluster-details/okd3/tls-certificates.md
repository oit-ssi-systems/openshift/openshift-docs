# Using TLS Certificates with Duke OKD

_Updated: 2019-02-18_

In addition to regular HTTP routes, OKD supports [Secured Routes](https://docs.okd.io/latest/architecture/networking/routes.html#secured-routes), which make use of TLS certificates to secure connections between the application and end-users.

There are three possible types of secured routes:

1. Edge Termination
2. Passthrough Termination
3. Re-encryption Termination

!!! note
    The finer details of the types of secured routes and their configuration are beyond the scope of this document.  See the [OKD Secured routes](https://docs.okd.io/latest/architecture/networking/routes.html#secured-routes) documentation for more info.

Secured Routes within Duke OKD may make use of Duke TLS certificates.

## Wild-card TLS Certificate

Routes within OKD that use a `*.cloud.duke.edu` domain name may make use of the wild-card TLS certifcate for `*.cloud.duke.edu` (a default cert provided by the cluster automatically), if it meets **either** of the following criteria:

- The route uses Edge Termination
- The route uses re-encrypt termination AND the back-end pod has a certificate signed by InCommon

## TLS for non-cloud.duke.edu domains or Passthrough termination

If your route is using [Passthrough Termination](https://docs.okd.io/latest/architecture/networking/routes.html#passthrough-termination), or a non-cloud.duke.edu domain, you will need to take extra steps to use TLS certificates.

### Passthrough

Regardless of the domain you are using, if you route uses Passthrough termination, you will need to manage the TLS certificates yourself.  The back-end pod where TLS traffic ends up must be configured to serve the certificate for the domain that matches your route.

For example, if your pod is running Apache, and your route is "example.oit.duke.edu", and your route is a passthrough route, then Apache must be configured to serve TLS requests for "example.oit.duke.edu", and contain a key and certificate that matches that domain.

### Non-cloud.duke.edu routes

If your route is setup for a domain other than *.cloud.duke.edu, you will need to get a key and certificate for your service.  For domains within duke.edu, you may be able to use [Locksmith](https://locksmith.oit.duke.edu/) to generate the key and certificate.

These keys and certs must be manually added to the routes, either in the YAML object, or via the web UI.  Check out the [OKD Developer Documentation on Routes](https://docs.okd.io/latest/dev_guide/routes.html) for more information on adding TLS certificates manually.

Alternatively, you can use the [SSL Certificate Automation service](/user-guide/locksmith-ssl-certificate-automation/) provided by Duke OKD.
