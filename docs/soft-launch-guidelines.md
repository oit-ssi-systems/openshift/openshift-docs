# OKD4 Soft Launch Guidelines

During the Soft Launch phase it is recommended that you familiarize yourself with the changes in OKD4 and begin with lower impact, less production critical workloads while we develop experience with the new version of this platform. The platform is expected to be stable but there are likely configuration scenarios and performance bottlenecks yet to be discovered that we would like to work through with less production critical workloads. The normal channels are available to help with this transition:

 - [Getting Help](getting-help.md)
 - [Duke Openshift Users Group](https://teams.microsoft.com/l/channel/19%3a8fc117cd5cf14856860c1b366507ad95%40thread.skype/General?groupId=f66622b8-ed9e-4fa4-927e-137089e86a0f&tenantId=cb72c54e-4a31-4d9e-b14a-1ea36dfac94c) 