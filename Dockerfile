FROM registry.access.redhat.com/ubi9/nginx-120:latest

USER 0
RUN dnf update -y
RUN dnf install -y python3-pip git
RUN pip3 install mkdocs-material Pygments mkdocs-git-revision-date-localized-plugin

COPY . .
RUN mkdocs build --clean --site-dir /tmp/src
RUN /usr/libexec/s2i/assemble
ENTRYPOINT /usr/libexec/s2i/run
USER 1001